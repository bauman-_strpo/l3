/* 
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

Программа-заготовка для домашнего задания
*/

#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"
#include "map"

const size_t MAX_NAME_LENGTH    = 50;
const size_t MIN_AMOUNT    = 0;

bool isCorrectMetalType(const std::string&);

class Robbery
{
    int _amount_stolen;
    float _cost;
    int _year, _month, _day;

public:
    Robbery() = delete;
    Robbery(int year, int month, int day, int amount_stolen, float cost);

    int getYear() const;
    int getMonth() const;
    int getDay() const;
    int getAmountStolen() const;
    float getCost() const;
}; 

class Coin : public ICollectable
{
    std::string _name;
    uint16_t _amount_total;
    std::vector<Robbery> _robberies;

protected:
    bool invariant() const;

public:
    Coin() = delete;
    Coin(const Coin & p) = delete;

    Coin & operator = (const Coin & p) = delete;

    Coin(const std::string & name, uint16_t amount_total);
    Coin(const std::string & name, uint16_t amount_total, std::vector<Robbery> robberies);

    const std::string & getName() const;
    uint16_t       getAmountTotal() const;
    const std::vector<Robbery>& getRobberies() const;
    void setRobberies(const std::vector<Robbery> & robberies);

    virtual bool   write(std::ostream& os) override;
};

class ItemCollector: public ACollector
{
public:
    virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif // HW_L3_DOMAIN_LAYER_H
