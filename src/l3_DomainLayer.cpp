/* Программа-заготовка для домашнего задания
*/

#include "hw/l3_DomainLayer.h"

using namespace std;

Robbery::Robbery(int year, int month, int day, int amount_stolen, float cost)
        : _year(year), _month(month), _day(day), _amount_stolen(amount_stolen), _cost(cost)
        {}

int Robbery::getYear() const 
{
    return _year;
}

int Robbery::getMonth() const 
{
    return _month;
}

int Robbery::getDay() const 
{
    return _day;
}

int Robbery::getAmountStolen() const 
{
    return _amount_stolen;
}

float Robbery::getCost() const 
{
    return _cost;
}

bool Coin::invariant() const
{
    return _amount_total >= MIN_AMOUNT;
}

Coin::Coin(const std::string & name, uint16_t amount_total)
    : _name(name), _amount_total(amount_total)
{
    if (!invariant()) throw Exception("Coin::Coin1","invariant");
}

Coin::Coin(const std::string & name, uint16_t amount_total, vector<Robbery> robberies)
    : _name(name), _amount_total(amount_total), _robberies(robberies)
{
    if (!invariant()) throw Exception("Coin::Coin2","invariant");
}

const string & Coin::getName() const
{
    return _name;
}

uint16_t Coin::getAmountTotal() const
{
    return _amount_total;
}

const std::vector<Robbery>& Coin::getRobberies() const
{
    return _robberies;
}

void Coin::setRobberies(const std::vector<Robbery> & robberies)
{
    _robberies = robberies;
}

bool   Coin::write(ostream& os)
{
    writeString(os, _name);
    writeNumber(os, _amount_total);
    
    size_t robberries_count = _robberies.size();
    writeNumber(os, robberries_count);

    for(const Robbery & r : _robberies)
    {
        writeNumber(os, r.getYear());
        writeNumber(os, r.getMonth());
        writeNumber(os, r.getDay());
        writeNumber(os, r.getAmountStolen());
        writeNumber(os, r.getCost());
    };

    return os.good();
}



shared_ptr<ICollectable> ItemCollector::read(istream& is)
{
    string   name = readString(is, MAX_NAME_LENGTH);
    uint16_t amount_total       = readNumber<uint16_t>(is);

    size_t robbery_count = readNumber<size_t>(is);
    vector<Robbery> r;

    r.reserve(robbery_count);
    for (size_t i=0; i < robbery_count; i++) {
        int year = readNumber<int>(is);
        int month = readNumber<int>(is);
        int day = readNumber<int>(is);
        int amount_total = readNumber<int>(is);
        int cost = readNumber<float>(is);

        r.push_back(Robbery(year,month,day,amount_total,cost));
    }

    return std::make_shared<Coin>(name, amount_total, r);
}
