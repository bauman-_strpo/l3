/* Программа-заготовка для домашнего задания
*/

#include "hw/l2_ApplicationLayer.h"

using namespace std;

inline const string DATA_DEFAULT_NAME = "lab.data";

bool Application::performCommand(int session_id, const vector<string> & args)
{
    if (args.empty())
        return false;

    LogSession log(session_id, args[0]);

    try
    {
        if (args[0] == "l" || args[0] == "load")
        {
            string filename = (args.size() == 1) ? DATA_DEFAULT_NAME : args[1];

            if (!_col.loadCollection(filename))
            {
                error(log, "Ошибка при загрузке файла '" + filename + "'");
                return false;
            }

            return true;
        }

        if (args[0] == "s" || args[0] == "save")
        {
            string filename = (args.size() == 1) ? DATA_DEFAULT_NAME : args[1];

            if (!_col.saveCollection(filename))
            {
                error(log, "Ошибка при сохранении файла '" + filename + "'");
                return false;
            }

            return true;
        }

        if (args[0] == "c" || args[0] == "clean")
        {
            if (args.size() != 1)
            {
                error(log, "Некорректное количество аргументов команды clean");
                return false;
            }

            _col.clean();

            return true;
        }

        if (args[0] == "a" || args[0] == "add")
        {
            if (args.size() != 3)
            {
                error(log, "Некорректное количество аргументов команды add");
                return false;
            }

            _col.addItem(make_shared<Coin>(args[1].c_str(), stoul(args[2])));
            return true;
        }

        if (args[0] == "ar" || args[0] == "add_robbery")
        {
            if (args.size() != 7)
            {
                error(log, "Некорректное количество аргументов команды add_robbery");
                return false;
            }

            Coin & coin = static_cast<Coin &>(*_col.getItem(stoul(args[1])));

            vector<Robbery> robberies = coin.getRobberies();
            robberies.push_back(Robbery(stoul(args[2]),stoul(args[3]),stoul(args[4]),stoul(args[5]),stof(args[6])));
            coin.setRobberies(robberies);
            return true;
        }


        if (args[0] == "r" || args[0] == "remove")
        {
            if (args.size() != 2)
            {
                error(log, "Некорректное количество аргументов команды remove");
                return false;
            }

            _col.removeItem(stoul(args[1]));
            return true;
        }

        if (args[0] == "u" || args[0] == "update")
        {
            if (args.size() != 4)
            {
                error(log, "Некорректное количество аргументов команды update");
                return false;
            }

            if (stoul(args[1]) >= _col.getSize())
            {
                error(log, "Некорректное значение индекса");
                return false;
            }

            _col.updateItem(stoul(args[1]), make_shared<Coin>(args[2].c_str(), stoul(args[3])));

            return true;
        }

        if (args[0] == "v" || args[0] == "view")
        {
            if (args.size() != 1)
            {
                error(log, "Некорректное количество аргументов команды view");
                return false;
            }

            size_t count = 0;
            for(size_t i=0; i < _col.getSize(); ++i)
            {
                const Coin & item = static_cast<Coin &>(*_col.getItem(i));

                if (!_col.isRemoved(i))
                {
                    _out.Output("[" + to_string(i) + "] "
                            + item.getName() + " "
                            + to_string(item.getAmountTotal()));

                    for (const Robbery & r : item.getRobberies())
                    {
                        _out.Output("\t" + to_string(r.getYear()) + " "
                                         + to_string(r.getMonth()) + " " 
                                         + to_string(r.getDay()) + " "
                                         + to_string(r.getAmountStolen()) + " "
                                         + to_string(r.getCost()));
                    }

                    count ++;
                }
            }

            log.information("Количество элементов в коллекции: " + to_string(count));
            return true;
        }

        if (args[0] == "rp" || args[0] == "report")
        {
            if (args.size() != 1)
            {
                    error(log, "Некорректное количество аргументов команды report");
                    return false;
            }

            double Total = 0;

            for(size_t i=0; i < _col.getSize(); ++i)
            {
                const Coin & item = static_cast<Coin &>(*_col.getItem(i));

                if (!_col.isRemoved(i))
                {
                    for (const Robbery & r : item.getRobberies())
                    {
                        Total += r.getAmountStolen() * r.getCost();
                    }
                }
            }

            error(log, "Страховой выплате подлежит " + to_string(Total));
            return true;
        }
    }
    catch(const Exception& e)
    {
        error(log, "Произошло исключение при обработке оператора " + args[0] +
            ": " + e.what());
    }
    catch(...)
    {
        error(log, "Произошло исключение при обработке оператора " + args[0]);
    }
    error(log, "Недопустимая команда '" + args[0] + "'");
    return false;
}

void Application::error(LogSession& log, string text){
    log.error(text);
    _out.Output(text);
}